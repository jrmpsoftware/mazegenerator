#pragma once

#include <iostream>
#include <string>
#include <time.h>
#include <stdlib.h>
#include <memory.h>
#include <stdint.h>
#include <fstream>
#include <random>
#include <array>

/**
* Maze.h
*
*
* Depth-first Search Random Maze Generator
*/

////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////

#if defined( __GNUC__ )
# define GCC_PACK(n) __attribute__((packed,aligned(n)))
#else
# define GCC_PACK(n) __declspec(align(n))
#endif // __GNUC__

#pragma pack(push, 1)
struct GCC_PACK(1) sBMPHeader
{
	// BITMAPFILEHEADER
	unsigned short bfType;
	uint32_t bfSize;
	unsigned short bfReserved1;
	unsigned short bfReserved2;
	uint32_t bfOffBits;
	// BITMAPINFOHEADER
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	unsigned short biPlanes;
	unsigned short biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
};
#pragma pack(pop)

enum eDirection
{
	eDirection_Invalid = 0,
	eDirection_Up = 1,
	eDirection_Right = 2,
	eDirection_Down = 4,
	eDirection_Left = 8
};

const int ImageSize = 256;
const int NumCells = 62;

/*
std::random_device rd;
std::mt19937 gen(rd());
std::uniform_int_distribution<> dis(0, NumCells - 1);
std::uniform_int_distribution<> dis4(0, 3);
*/

class Maze
{

public:


	//                   0  1  2  3  4  5  6  7  8
	//                      U  R     D           L
	//int Heading_X[9] = { 0, 0, +1, 0, 0, 0, 0, 0, -1 };
	//int Heading_Y[9] = { 0, -1, 0, 0, +1, 0, 0, 0, 0 };
	//int Mask[9] = {
	//	0,
	//	eDirection_Down | eDirection_Down << 4,
	//	eDirection_Left | eDirection_Left << 4,
	//	0,
	//	eDirection_Up | eDirection_Up << 4,
	//	0,
	///	0,
	//	0,
	//	eDirection_Right | eDirection_Right << 4
	//};

	std::array<int, 9> Heading_X;
	std::array<int, 9> Heading_Y;
	std::array<int, 9> Mask;

	/////////////////Maze parameters
	const char*						Version; 
	int								CellSize;
	unsigned char*					g_Maze; 

	std::random_device				rd;
	std::mt19937					gen;
	std::uniform_int_distribution<> dis;
	std::uniform_int_distribution<> dis4;


	// current traversing position
	int g_PtX;
	int g_PtY;


	Maze();
	~Maze();

	// return a random integer
	int RandomInt();
	int RandomInt4();

	// return the current index in g_Maze
	int CellIdx();

	// check for valid direction
	bool IsDirValid(eDirection Dir);
	eDirection GetDirection();

	// Generate maze
	void GenerateMaze();

	// define maze lines
	void Line(unsigned char* img, int x1, int y1, int x2, int y2);

	// draw maze
	void RenderMaze(unsigned char* img);

	// save to bmp
	void SaveBMP(const char* FileName, const void* RawBGRImage, int Width, int Height);

	// print app info
	void PrintInfo();
};

