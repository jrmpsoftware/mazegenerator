// MazeGenerator_Console.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Maze.h"


int _tmain(int argc, _TCHAR* argv[])
{
	Maze mMaze;
	char* mString = "";

	mMaze.PrintInfo();

	std::cout << "Generating " << NumCells << " x " << NumCells << " maze into " << ImageSize << " x " << ImageSize << " bitmap" << std::endl;

	// prepare PRNG
	mMaze.gen.seed(time(NULL));

	// clear maze
	std::fill(mMaze.g_Maze, mMaze.g_Maze + NumCells * NumCells, 0);

	// setup initial point
	mMaze.g_PtX = mMaze.RandomInt();
	mMaze.g_PtY = mMaze.RandomInt();

	// traverse
	mMaze.GenerateMaze();

	// prepare BGR image
	size_t DataSize = 3 * ImageSize * ImageSize;

	unsigned char* Img = new unsigned char[DataSize];

	memset(Img, 0, DataSize);

	// render maze on bitmap
	mMaze.RenderMaze(Img);

	mMaze.SaveBMP("Maze.bmp", Img, ImageSize, ImageSize);
	system("Maze.bmp");

	// cleanup
	delete[](Img);

	//
	//std::cin >> mString;

	return 0;
}

