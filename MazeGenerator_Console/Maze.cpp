#include "Maze.h"


/**
* Maze.cpp
* 
*
* Depth-first Search Random Maze Generator
*/

/////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////////////

Maze::Maze() : Heading_X({ 0, 0, +1, 0, 0, 0, 0, 0, -1 }), 
				Heading_Y({ 0, -1, 0, 0, +1, 0, 0, 0, 0 }), 
				Mask({ 0,
				eDirection_Down | eDirection_Down << 4,
				eDirection_Left | eDirection_Left << 4,
				0,
				eDirection_Up | eDirection_Up << 4,
				0,
				0,
				0,
				eDirection_Right | eDirection_Right << 4 })
{
	CellSize = ImageSize / NumCells;
	g_Maze = new unsigned char[NumCells* NumCells];

	gen =  std::mt19937(rd());
	dis = std::uniform_int_distribution<>(0, NumCells - 1);
	dis4 = std::uniform_int_distribution<>(0, 3);

	Version = "1.0";
}


Maze::~Maze()
{
}



// return the current index in g_Maze
int Maze::CellIdx()
{
	return g_PtX + NumCells * g_PtY;
}



int Maze::RandomInt()
{
	return static_cast<int>(dis(gen));
}

int Maze::RandomInt4()
{
	return static_cast<int>(dis4(gen));
}


bool Maze::IsDirValid(eDirection Dir)
{
	int NewX = g_PtX + Heading_X[Dir];
	int NewY = g_PtY + Heading_Y[Dir];

	if (!Dir || NewX < 0 || NewY < 0 || NewX >= NumCells || NewY >= NumCells) return false;

	return !g_Maze[NewX + NumCells * NewY];
}

eDirection Maze::GetDirection()
{
	eDirection Dir = eDirection(1 << RandomInt4());

	while (true)
	{
		for (int x = 0; x < 4; x++)
		{
			if (IsDirValid(Dir)) { return eDirection(Dir); }

			Dir = eDirection(Dir << 1);

			if (Dir > eDirection_Left) { Dir = eDirection_Up; }
		}

		Dir = eDirection((g_Maze[CellIdx()] & 0xf0) >> 4);

		// nowhere to go
		if (!Dir) return eDirection_Invalid;

		g_PtX += Heading_X[Dir];
		g_PtY += Heading_Y[Dir];

		Dir = eDirection(1 << RandomInt4());
	}
}

void Maze::GenerateMaze()
{
	int Cells = 0;

	for (eDirection Dir = GetDirection(); Dir != eDirection_Invalid; Dir = GetDirection())
	{
		// a progress indicator, kind of
		if (++Cells % 1000 == 0) std::cout << ".";

		g_Maze[CellIdx()] |= Dir;

		g_PtX += Heading_X[Dir];
		g_PtY += Heading_Y[Dir];

		g_Maze[CellIdx()] = Mask[Dir];
	}

	std::cout << std::endl;
}


void Maze::SaveBMP(const char* FileName, const void* RawBGRImage, int Width, int Height)
{
	sBMPHeader Header;

	int ImageSize = Width * Height * 3;

	Header.bfType = 0x4D * 256 + 0x42;
	Header.bfSize = ImageSize + sizeof(sBMPHeader);
	Header.bfReserved1 = 0;
	Header.bfReserved2 = 0;
	Header.bfOffBits = 0x36;
	Header.biSize = 40;
	Header.biWidth = Width;
	Header.biHeight = Height;
	Header.biPlanes = 1;
	Header.biBitCount = 24;
	Header.biCompression = 0;
	Header.biSizeImage = ImageSize;
	Header.biXPelsPerMeter = 6000;
	Header.biYPelsPerMeter = 6000;
	Header.biClrUsed = 0;
	Header.biClrImportant = 0;

	std::ofstream File(FileName, std::ios::out | std::ios::binary);

	File.write((const char*)&Header, sizeof(Header));
	File.write((const char*)RawBGRImage, ImageSize);

	std::cout << "Saved " << FileName << std::endl;
}

void Maze::Line(unsigned char* img, int x1, int y1, int x2, int y2)
{
	if (x1 == x2)
	{
		// vertical line
		for (int y = y1; y < y2; y++)
		{
			if (x1 >= ImageSize || y >= ImageSize) continue;
			int i = 3 * (y * ImageSize + x1);
			img[i + 2] = img[i + 1] = img[i + 0] = 255;
		}
	}

	if (y1 == y2)
	{
		// horizontal line
		for (int x = x1; x < x2; x++)
		{
			if (y1 >= ImageSize || x >= ImageSize) continue;
			int i = 3 * (y1 * ImageSize + x);
			img[i + 2] = img[i + 1] = img[i + 0] = 255;
		}
	}
}

void Maze::RenderMaze(unsigned char* img)
{
	for (int y = 0; y < NumCells; y++)
	{
		for (int x = 0; x < NumCells; x++)
		{
			char v = g_Maze[y * NumCells + x];

			int nx = x * CellSize;
			int ny = y * CellSize;

			if (!(v & eDirection_Up)) Line(img, nx, ny, nx + CellSize + 1, ny);
			if (!(v & eDirection_Right)) Line(img, nx + CellSize, ny, nx + CellSize, ny + CellSize + 1);
			if (!(v & eDirection_Down)) Line(img, nx, ny + CellSize, nx + CellSize + 1, ny + CellSize);
			if (!(v & eDirection_Left)) Line(img, nx, ny, nx, ny + CellSize + 1);
		}
	}
}

void Maze::PrintInfo()
{
	std::cout << "Depth-first Search Random Maze Generator" << std::endl;
	std::cout << "Version " << Version << std::endl;
	std::cout << std::endl;
}
